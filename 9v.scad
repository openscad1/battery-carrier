include <libopenscad/mcube.scad>
include <libopenscad/config.scad>

include <config.scad>


module 9v_slug() {
    color("orange") {
        mcube([nine_volt_width, nine_volt_depth, nine_volt_height], center = true, chamfer = 1);
    }
    


    translate([0, 0, nine_volt_height / 2]) {
        $fn = 50;
        // negative terminal
        translate([-nine_volt_width / 4, 0, 0]) {
            difference() {
                cylinder(d=9, h = 3*2, center = true);
                cylinder(d=5, h=10, center = true);
            }
        }
        
        // positive terminal
        translate([ nine_volt_width / 4, 0, 0]) {
            cylinder(d=6, h = 3*2, center = true);
        }

    }
}

9v_slug();
